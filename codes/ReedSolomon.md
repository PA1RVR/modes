# Reed Solomon error detection / correction

> *How to encode k symbols into a (longer) series of n symbols in the same alphabet.*



## Links

  * [Interactive demo](https://sigh.github.io/reed-solomon/)
