# FT4 Digital Mode

> *FT4 is a variant to FT8 with fewer symbols (4 instead of 8) and more speed for
> more bandwidth (90 Hz instead of 50 Hz).  You take some, you lose some.*

**Note:** The descriptive / specifying information on FT4 is really scattered,
and usually incomplete.  This is a reconstruction of what I could find.

SRC.1 and SRC.2 are online sources, clearly non-authoritative.
SRC.3 seems more coherent, *The FT4 and FT8 Communication Protocols* by K9AN, G4WJS, K1JT.


## Symbols Transmitted and their Timing

  * FT4 uses 4 symbols 0..3 with 23.4 Hz separation.  It sends a total of 105 symbols.

  * SRC.1: FT4 transmits for 6.3 seconds, as part of a 7.5 second window.
  * SRC.2: FT4 transmits for 4.48 seconds, as part of a ?.? second window.

  * SRC.1: This leaves 60 ms per symbol, or a baud rate of 1/0.060 = 16.667
  * SRC.2: This leaves 42.667 ms per symbol, or a baud rate of 23.4375 baud
  * SRC.3: There are 48 ms per symbol, or a baud rate of 1/0.048 = 20.833 baud

  * SRC.3: FT4 transmits for 0.048 s/sym * 105 sym = 5.040 seconds


## Symbols Transmitted

Recollected from `ft4code` output (a really poor substitute for a spec):

  * Ramp symbol `0`
  * Sync symbols `0132` forms a Costas array
  * A sequence of 29 data/correction symbols
  * Sync symbols `1023` forms a Costas array
  * A sequence of 29 data/correction symbols
  * Sync symbols `2310` forms a Costas array
  * A sequence of 29 data/correction symbols
  * Sync symbols `3201` forms a Costas array
  * Ramp symbol `0`

The total length is 1+4+29+4+29+4+29+4+1 = 105 symbols.
