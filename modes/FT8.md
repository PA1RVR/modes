# FT8 Digital Mode

> *FT8 can reach long distances with low power; for instance, connecting Europa to America at 1 Watt.*

  * The bitrate is said to be *6.25 b/s* in a *50 Hz* bandwidth
    (Really?  I get to *77b/15s = 5.133 b/s* because a 77-bit frame sends in 15-second window!)
    (In fact, the symbol rate while sending is *6.25 sym/s = 6.25 baud* but that's not bits per second!)
  * The transmit window occupies 12.64s in a 15s clock-synchronised schedule


## QSO Specialist

The only purpose of FT8 is making QSO's, so a bare minimum contact:

  - `CQ <caller> <mhloc>`: CQ call with Maidenhead AZ09 location
  - `<caller> <callee> <mhloc>`: reply with AZ09 location
  - `<callee> <caller> <snr>`: response with signal report (dB-level, e.g. -12)
  - `<caller> <callee> R<snr>`: roger/received with signal report
  - `<callee> <caller> RR73`: report received, best regards
  - `<caller> <callee> 73`: best regards

The general format is `<to> <from> <message>` with an exception for `CQ`.
There is a free text mode 0.0 for FT8 traffic.
For more elaborate communication, such as keyboard chat, see JS8Call which builds on this mode.


## Encoding and Decoding

**Coding example** running on the commandline:

```
shell$ ft8code 'Hi Hot World'
    Message                               Decoded                             Err i3.n3
----------------------------------------------------------------------------------------------------
 1. HI HOT WORLD                          HI HOT WORLD                             0.0 Free text                   

Source-encoded message, 77 bits: 
00000001011011110110100011101001110101110101011101011010011000011010010000000

14-bit CRC: 
00101000101110

83 Parity bits: 
11110100001000111001101001010011000010111111001000000101101000100101000000000011001

Channel symbols (79 tones):
  Sync               Data               Sync               Data               Sync
3140652 00347660732626373454045500353 3140652 72451071613203771006605600021 3140652
```

**Data comparison** shows that word encoding is not trivial; mapping to uppercase suggests a 6-bit charset:

  * `HELLO = 00000000000000000000000000000000000000000000011011010000011011110000101000000`
  * `HALLO = 00000000000000000000000000000000000000000000011011000111011000111100101000000`


**Encoding FT8** uses steps that aim for low power and high recoverability:

  - Compress the message, while being aware of the message format
  - Pack the message into frames of up to **77 bits**
  - Add a 14-bit CRC to come to **91 bits**
  - Add 83 parity bits with LDPC to come to **174 bits**
  - Encode 3 bits at a time into **58 symbols**
  - Split into halves of 29 symbols, embed between `3140652` sync to form **79 tones**
  - Produce tones using Gaussian FSK-8, with *160 ms* per tone, *6.52 Hz* between tones for a bandwidth of *50 Hz*
  - Transmit the tones as [USB with 3 kHz bandwidth](https://www.sigidwiki.com/wiki/File:Multiple_FT8_signals-in-3k-audio-bandwidth_40m.jpg)


**Decoding FT8** involves taking in the entire USB audio signal, mapping it with FFT and
seeking out patterns that could be a synchronisation, then analysing contents.
The FFT looks at steps of *3.125 Hz*, so at a bandwidth of *200-2500 Hz* that means *N=1+(2500-200)/3.125=737$.

The sync uses an interesting trick; it serves to sync both frequency and time with a
7x7 Costas Array](https://laarc.weebly.com/uploads/7/3/2/9/73292865/techfest_2019_wb2fko_revised.pdf)
that is chosen to be `3140652` for FT8.  There is a white paper by WB2FKO on
[Synchronization in FT8](http://www.sportscliche.com/wb2fko/FT8sync.pdf).

Searching for Costas Arrays requires FFTs with overlap of, say, 40ms distance
for 1/4 symbol period.  Scanning from -2s to +3s calls for 126 time steps in 737 frequency steps = 92862 searches for Costas arrays.


## Low Power

The transmission power is low because only one sine wave is transmitted at any time.
Compare that to the multiple-sine transmissions with AM.

The power per information bit of stretched over time, as well as over repetitions.

To broadcast 77 bits of information, the maximum in a frame, it needs to transmit 12.64s.
When this is done at 1 Watt, the energy per bit is *12.64s · 1W / 77b = 164.2mJ/b*.

Decoding may also be somewhat constly, and should be multiplied by the number of receivers.


## Signal-to-Noise Ratio

It looks like the signals are weaker than the noise.
In reality, the signal has a small bandwidth compared to the audio channel.

To reduce the noise power over a bandwidth of 2500 Hz to one for the 50 Hz for the FSK-8 underlying mode, add *10.log(2500/50) ≈ 17 dB* to the reported SNR.

The reception of the signals is good thanks to two measures:

  - Transmitting a sine wave for a fair bit of time
  - LDPC as a FEC, and a 14-bit Checksum for validation


## Experiments with ft8modem

  - Need `librtaudio-dev` as documented
  - Need `libsndfile1-dev` on top of that

Run modem (on device 0):

  - `./ft8modem ft8 0`

Make a WAV:

  - `./ft8encode ft8 44100 1200 /tmp/out.wav 'Hello World'`

Decode a WAV:

  - ?? `./test_decode /tmp/out.wav`
  - Tried without luck

Connect with a loopback device (ft8modem, wsjtx, js8call); the commands seem to wait for the programs to come online:

```
pw-loopback -C Firefox -P 'ALSA plug-in [ft8modem]' &   # ft8modem
pw-loopback -C Firefox -P 'QtPulseAudio:91880' &        # JS8Call
pw-loopback -C Firefox -P 'ALSA plug-in [wsjtx]' &      # WSJT-X
```

Monitor what happens with

```
pw-top
```


## Links

Information:

  - [WSJT info on TF8 and TF4](https://wsjt.sourceforge.io/FT4_FT8_QEX.pdf)
  - [SigIDwiki on FT8](https://www.sigidwiki.com/wiki/FT8)
  - [WikiPedia on FT8](https://en.wikipedia.org/wiki/FT8)
  - [IEEE slides on FT8 by Dale Burmester KA9SWE](https://site.ieee.org/msn/files/2019/04/FT8-KA9SWE.pdf)
  - [Basics of FT8 by Essex HAM](https://www.essexham.co.uk/ft8-basics-explained)
  - [FT8 Frequencies](https://www.dxzone.com/ft8-frequencies/) with special ones for JS8Call
  - [ft8code(1)](https://manpages.debian.org/bookworm/wsjtx/ft8code.1.en.html)
  - [pavucontrol](https://wiki.debian.org/audio-loopback) may be used for audio loopbacks under [PulseAudio](https://wiki.archlinux.org/title/PulseAudio/Examples) and may be [used with a source](https://wiki.archlinux.org/title/PulseAudio/Examples#Pipe_a_source_directly_into_a_sink) like [Pieter Tjerk's WebSDR](http://websdr.ewi.utwente.nl:8901/) &mdash; but it may also work with [`pw-loopback`](https://docs.pipewire.org/page_man_pw-loopback_1.html) because FireFox, JS8Call and `ft8-modem` all work through PipeWire and show up in `pw-top`.
  - [TED on ugly music by Costa Arrays](https://www.ted.com/talks/scott_rickard_the_beautiful_math_behind_the_world_s_ugliest_music)

Software:

  - [FT8Code]()
  - [FT8modem](http://www.kk5jy.net/ft8modem/) is a commandline wrapper around utilities from WSJT-X by KY55JY under GPLv3 with [source code](http://www.kk5jy.net/ft8modem/Software/) with dependencies `apt install librtaudio-dev libsndfile1-dev`
  - [JS8Call](http://js8call.com/) is the chat mode on top of the FT8 mode
  - [JTDX]()
  - [FT8_Lib](https://github.com/kgoba/ft8_lib) recodes the algorithm
  - [WSJT-X](https://wsjt.sourceforge.io/wsjtx.html) under GPLv3 so with [source code](https://sourceforge.net/p/wsjt/wsjtx/ci/master/tree/)
  - [WSJTEncoder](https://github.com/eicket/myWSJTEncoder) written in Java, 

