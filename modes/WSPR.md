# WSPR (whisper)


## Links

  * [WSPR on WikiPedia](https://en.wikipedia.org/wiki/WSPR_(amateur_radio_software))
  * [WSPR protocol specification](https://swharden.com/software/FSKview/wspr/)


## Symbol Transmission

  * There are 4 symbols numbered 0..3.
  * The symbols are frequencies with 1.4648 Hz separation, good for a bandwidth ≈ 6 Hz.
  * The transmission time per symbol is 12000/8192 ≈ 1.4648 baud.
  * A transmission contains 162 symbols, and lasts 162 * 12000 / 8192 = 110.6 seconds.
  * Transmission starts at a UTC even minute + 1 second.
  * Note that this aligns all WSPR transmissions, with a margin for timing differences and delays.

To encode a message:

```
wsprcode 'PA1RVR AB34 37'
Message: PA1RVR AB34 37        

Source-encoded message, 50 bits:
Hex:     AA D9 4B 8F 79 D9 40
Binary:  10101010 11011001 01001011 10001111 01111001 11011001 01

Data symbols:
      1 1 0 0 0 1 0 0 1 0 0 1 0 1 0 1 0 0 0 0 0 0 1 1 0 1 1 0 0 0
      1 0 1 0 0 0 1 0 0 0 0 1 0 0 0 1 1 1 0 0 1 1 0 0 1 0 0 1 0 0
      0 1 0 1 0 0 0 1 1 0 0 1 1 1 1 0 0 0 1 0 0 1 0 0 0 1 0 0 0 1
      1 0 0 1 1 1 0 0 1 1 1 0 1 0 1 1 0 1 1 0 0 0 0 0 0 1 0 0 0 0
      1 1 0 1 0 0 1 1 1 0 0 0 0 1 0 1 1 1 0 0 0 0 0 1 1 0 0 1 0 0
      0 1 1 0 1 0 1 0 1 1 1 1

Sync symbols:
      1 1 0 0 0 0 0 0 1 0 0 0 1 1 1 0 0 0 1 0 0 1 0 1 1 1 1 0 0 0
      0 0 0 0 1 0 0 1 0 1 0 0 0 0 0 0 1 0 1 1 0 0 1 1 0 1 0 0 0 1
      1 0 1 0 0 0 0 1 1 0 1 0 1 0 1 0 1 0 0 1 0 0 1 0 1 1 0 0 0 1
      1 0 1 0 1 0 0 0 1 0 0 0 0 0 1 0 0 1 0 0 1 1 1 0 1 1 0 0 1 1
      0 1 0 0 0 1 1 1 0 0 0 0 0 1 0 1 0 0 1 1 0 0 0 0 0 0 0 1 1 0
      1 0 1 1 0 0 0 1 1 0 0 0

Channel symbols:
      3 3 0 0 0 2 0 0 3 0 0 2 1 3 1 2 0 0 1 0 0 1 2 3 1 3 3 0 0 0
      2 0 2 0 1 0 2 1 0 1 0 2 0 0 0 2 3 2 1 1 2 2 1 1 2 1 0 2 0 1
      1 2 1 2 0 0 0 3 3 0 1 2 3 2 3 0 1 0 2 1 0 2 1 0 1 3 0 0 0 3
      3 0 1 2 3 2 0 0 3 2 2 0 2 0 3 2 0 3 2 0 1 1 1 0 1 3 0 0 1 1
      2 3 0 2 0 1 3 3 2 0 0 0 0 3 0 3 2 2 1 1 0 0 0 2 2 0 0 3 1 0
      1 2 3 1 2 0 2 1 3 2 2 2

Decoded message: PA1RVR AB34 37           ntype: 37
```

