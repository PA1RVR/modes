#!/usr/bin/env python3
#
# Encode M input codes into N characters from an alphabet A.
# This is possible when M <= |A| ^ N, where excess adds redundancy.
#
# Aim for the highest possible Hamming distance.
#
# From: Rick van Rein PA1RVR <rick+PA1RVR@vanrein.org>


import sys
from math import log, ceil, floor


# THINKING OUT LOUD
#  - Distribute input code points in the output space, wide apart
#  - Place "wedges" that split the output space to form input code points
#  - Those "wedges" differ over as many dimensions as possible
#  - Given overlap in dimensions, choose mostly different output symbols
#  - Aim for output symbols maximally remote from "wedges" in all dimensions
#  - Would be nice to compute the maximum distances ahead of time (as in RS)
#  - Knowing maximum distances could simplify placement of "wedges"


# INITIAL ALGORITHM IDEA
#  - Stop distinguishing "data" from "error handling codes"
#  - Rather smear it all out over a continuous space
#  - Find N values Fi that are relative prime to M and each other
#  - In practice, find N primes Pi that follow after M
#  - Proposal     :- Output symbol Oi is ( I * Pi ) % M
#  - Conjecture 1 :- This yields maximum Hamming distance
#  - Conjecture 2 :- This can trivially be recovered
#  - To be proven :- This is a good alternative for Reed-Solomon codes
#  - Note however :- This involves work on all data, even the original

# REDUCE THE ENCODED LENGTH VARIABILITY
#  - Information I is an integer, 0 <= I < M, but encode I+M
#  - Matching this with a unknown-times-factor reduces the unknown value space



#
# SUPPORTING DEFINITIONS
#

errorflag = False
roundfault = 0.0001

def error (f):
	global errorflag
	errorflag = True
	print (f % s, file=sys.stderr)

def delayed_error_exit ():
	if errorflag:
		error ('Failures detected in execution')
		sys.exit (1)

def mkint (x):
	y = int (round (x))
	if not abs (y - x) < roundfault:
		error ('Internal Error: Non-trivial rounding fault %f' % (y-x,))
	return y



#
#
# TEST A SMALL EXAMPLE TO GROW SOME INTUITION
#
# USE DIFFERENT COMPUTATION METHODS FOR ROBUST COMPARISON
#
#


# Alphabet A with size szA
A = [ 0, 1, 2, 3, 4, 5, 6 ]
szA = len (A)

# Input space in M
M = 256
print ('M / |A| = %f%%' % (M / szA,))

# Prime for dimension i, j
P_i = 257
P_j = 263
print ('P_i / |A| = %f%% and rem = %d' % ( P_i / szA , P_i % szA ))
print ('P_j / |A| = %f%% and rem = %d' % ( P_j / szA , P_j % szA ))

# Original input origI
origI = 123

# Sample for dimenson i, j
#INITIAL#TEST# S_i = 125
#INITIAL#TEST# S_j = 106
S_i = ( ( origI + M ) * P_i ) % szA
S_j = ( ( origI + M ) * P_j ) % szA
print ('Sample values: %d,%d' % (S_i,S_j))

# Derive k_i, k_j minimum and maximum values
k_i_min = ceil  ( ( P_i *       M       - S_i ) / szA )
k_i_max = floor ( ( P_i * ( 2 * M - 1 ) - S_i ) / szA )
k_j_min = ceil  ( ( P_j *       M       - S_j ) / szA )
k_j_max = floor ( ( P_j * ( 2 * M - 1 ) - S_j ) / szA )

# Brute-force candidates I_i*
#TODO# Both cand0_I_? contain the result.  But both step with szA, I had expected different P_i / P_j steps.  True enough, they adapt their unknown, k_?_x, to produce a k_?_x * szA * P_? range which is then divided by P_? to produce szA stepping.  Which is the same for both, and therefore completely useless for filtering.
cand0_I_i = [ mkint ( ( S_i + k_i_x * szA ) / P_i ) - M
		for k_i_x in range (k_i_min, k_i_max + 1)
		if ( S_i + k_i_x * szA ) % P_i == 0 ]
cand0_I_j = [ mkint ( ( S_j + k_j_x * szA ) / P_j ) - M
		for k_j_x in range (k_j_min, k_j_max + 1)
		if ( S_j + k_j_x * szA ) % P_j == 0 ]
print ('Alg 0 candidate I_i*, #%d %f%% = %r' % (len(cand0_I_i),100.0*len(cand0_I_i)/szA,cand0_I_i))
print ('Alg 0 candidate I_j*, #%d %f%% = %r' % (len(cand0_I_j),100.0*len(cand0_I_j)/szA,cand0_I_j))

# Sanity checks on candidate values
for c in cand0_I_i:
	if c < 0 or c >= M:
		print ('Bad value for I_i* %d not in range [%d,%d>' % (c, 0, M))
for c in cand0_I_j:
	if c < 0 or c >= M:
		print ('Bad value for I_j* %d not in range [%d,%d>' % (c, 0, M))

# Derive the overlapping values of I_Ii* and I_j*
cand0_I_ij = [ x for x in cand0_I_i if x in cand0_I_j ]
print ('Alg 0 candidate I_i* ⋂ I_j*, #%d %f%% = %r' % (len(cand0_I_ij),100.0*len(cand0_I_ij)/szA,cand0_I_ij))


#
# Finish properly; report if we ran into errors
#
delayed_error_exit ()

